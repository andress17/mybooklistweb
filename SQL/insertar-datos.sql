INSERT INTO Roles(nombre) VALUES ("Autor"),("Ilustrador"),("Traductor");
INSERT INTO Editoriales (nombre) VALUES
  ("Desconocida"),
  ("Bloomsbury Publishing"),
  ("Editorial Planeta"),
  ("Alianza Editorial"),
  ("Editorial Salvat");
INSERT INTO Libros (titulo_ingles, titulo_original, isbn, portada, fecha_publicacion, sinopsis, puntuacion, editorial) VALUES
  ("Harry Potter and the Philosopher's Stone", "Harry Potter and the Philosopher's Stone","0747532745","assets/img/libros/harry-potter-and-the-philosophers-stone.jpg","1997-06-26","The story novels about Harry Potter, a boy who learns on his eleventh birthday that he is the orphaned son of two powerful wizards and possesses unique magical powers of his own. He is summoned from his life as an unwanted child to become a student at Hogwarts, an English boarding school for wizards. There, he meets several friends who become his closest allies and help him discover the truth about his parents' mysterious deaths.", 0.0, 2),
  ("Harry Potter and the Chamber of Secrets","Harry Potter and the Chamber of Secrets","0747538484","assets/img/libros/harry-potter-and-the-chamber-of-secrets.jpg","1998-07-02","The second instalment of boy wizard Harry Potter's adventures at Hogwarts School of Witchcraft and Wizardry. A mysterious elf tells Harry to expect trouble during his second year at Hogwarts, but nothing can prepare him for trees that fight back, flying cars, spiders that talk and deadly warnings written in blood on the walls of the school.",0.0,2),
  ("Harry Potter and the Prisoner of Azkaban","Harry Potter and the Prisoner of Azkaban","0747546290","assets/img/libros/harry-potter-and-the-prisoner-of-azkaban.jpg","1999-07-08","Harry Potter's third year at Hogwarts starts off badly when he learns deranged killer Sirius Black has escaped from Azkaban prison and is bent on murdering the teenage wizard. While Hermione's cat torments Ron's sickly rat, causing a rift among the trio, a swarm of nasty Dementors is sent to protect the school from Black. A mysterious new teacher helps Harry learn to defend himself, but what is his secret tie to Sirius Black?",0.0,2),
  ("Harry Potter and the Goblet of Fire","Harry Potter and the Goblet of Fire","074754624X","assets/img/libros/harry-potter-and-the-goblet-of-fire.jpg","2000-07-08","The fourth book in the Harry Potter franchise sees Harry returning for his fourth year at Hogwarts School of Witchcraft and Wizardry, along with his friends, Ron and Hermione. There is an upcoming tournament between the three major schools of magic, with one participant selected from each school by the Goblet of Fire. When Harry's name is drawn, even though he is not eligible and is a fourth player, he must compete in the dangerous contest.",0.0,2),
  ("Harry Potter and the Order of the Phoenix","Harry Potter and the Order of the Phoenix","0747551006","assets/img/libros/harry-potter-and-the-order-of-the-phoenix.jpg","2003-06-21","Now in his fifth year at Hogwarts, Harry learns that many in the wizarding community do not know the truth of his encounter with Lord Voldemort. Cornelius Fudge, minister of Magic, appoints his toady, Dolores Umbridge, as Defense Against the Dark Arts teacher, for he fears that professor Dumbledore will take his job. But her teaching is deficient and her methods, cruel, so Harry prepares a group of students to defend the school against a rising tide of evil.",0.0,2),
  ("Harry Potter and the Half-Blood Prince","Harry Potter and the Half-Blood Prince","0747581088","assets/img/libros/harry-potter-and-the-half-blood-prince.jpg","2005-07-16","As Death Eaters wreak havoc in both Muggle and Wizard worlds, Hogwarts is no longer a safe haven for students. Though Harry suspects there are new dangers lurking within the castle walls, Dumbledore is more intent than ever on preparing the young wizard for the final battle with Voldemort. Meanwhile, teenage hormones run rampant through Hogwarts, presenting a different sort of danger. Love may be in the air, but tragedy looms, and Hogwarts may never be the same again.",0.0,2),
  ("Where the Crawdads Sing","Where the Crawdads Sing","9780735219090","assets/img/libros/wherethecrawdadssing.jpg","2018-08-14","For years, rumors of the Marsh Girl have haunted Barkley Cove, a quiet town on the North Carolina coast. So in late 1969, when handsome Chase Andrews is found dead, the locals immediately suspect Kya Clark, the so-called Marsh Girl. But Kya is not what they say. Sensitive and intelligent, she has survived for years alone in the marsh that she calls home, finding friends in the gulls and lessons in the sand. Then the time comes when she yearns to be touched and loved. When two young men from town become intrigued by her wild beauty, Kya opens herself to a new life--until the unthinkable happens.Where the Crawdads Sing is at once an exquisite ode to the natural world, a heartbreaking coming-of-age story, and a surprising tale of possible murder. Owens reminds us that we are forever shaped by the children we once were, and that we are all subject to the beautiful and violent secrets that nature keeps.",0.0,2),
  ("Educated: A Memoir","Educated: A Memoir","9780399590504","assets/img/libros/educatedamemoir.jpg","2018-02-20","Educated is an account of the struggle for self-invention. It is a tale of fierce family loyalty and of the grief that comes with severing the closest of ties. With the acute insight that distinguishes all great writers, Westover has crafted a universal coming-of-age story that gets to the heart of what an education is and what it offers: the perspective to see one's life through new eyes and the will to change it.",0.0,2);

INSERT INTO `Generos`(`nombre`) VALUES ("Desconocido");

INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Desconocido/a","Desconocido/a", "assets/img/editoriales/desconocido.jpg", '1970-01-01');
INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Joanne","Rowling​", "assets/img/editoriales/jkrowling.jpg", '1965-06-30');
INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Carmen","Laforet", "assets/img/editoriales/carmenlaforet.jpg", '1921-09-06');
INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Ana","Frank", "assets/img/editoriales/anafrank.jpg", '1929-06-12');
INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Pablo","Neruda​", "assets/img/editoriales/pabloneruda.jpg", '1904-07-12');
INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Agatha","Christie​", "assets/img/editoriales/agathachristie.jpg", '1890-09-15');
INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Emily","Brontë", "assets/img/editoriales/emilybronte.jpg", '1818-07-30');
INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Delia","Owens", "assets/img/editoriales/deliaowens.jpg", '1949-01-01');
INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Michelle","Obama", "assets/img/editoriales/michelleobama.jpg", '1964-01-17');
INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Tara","Westover", "assets/img/editoriales/tarawestover.jpg", '1986-09-27');
INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Dav","Pilkey", "assets/img/editoriales/davpilkey.jpg", '1966-03-04');
INSERT INTO `Personas`(`nombre`, `apellidos`, `foto`, `fecha_nacimiento`) VALUES ("Rachel","Hollis", "assets/img/editoriales/rachelhollis.jpg", '1983-01-09');


SELECT id into @autorJK FROM Personas WHERE nombre='Joanne' AND apellidos='Rowling​';
SELECT id into @rolAutor FROM Roles WHERE nombre='Autor';

INSERT INTO Personas_Roles_Libros (rol, persona, libro) VALUES (@rolAutor,@autorJK,1);
INSERT INTO Personas_Roles_Libros (rol, persona, libro) VALUES (@rolAutor,@autorJK,2);
INSERT INTO Personas_Roles_Libros (rol, persona, libro) VALUES (@rolAutor,@autorJK,3);
INSERT INTO Personas_Roles_Libros (rol, persona, libro) VALUES (@rolAutor,@autorJK,4);

INSERT INTO Estados(nombre) VALUES('Leyendo'),('Completado'),('Pausado'),('Dejado'),('Planeado para leer');

INSERT INTO Tipos_Actividad_Usuarios(nombre) VALUES ("está leyendo"),("ha completado"),("ha pausado"),("ha dejado"),("planea para leer"),("ha puntuado un libro"),("ha escrito un analisis sobre"),("ha comentado en el perfil de"),("ha publicado una noticia");
