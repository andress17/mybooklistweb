const express = require('express');
const mariadb = require('mariadb/callback');
const crypto = require('crypto');
const router = express.Router();
const jwt = require('jsonwebtoken');


const conn = mariadb.createConnection({
    host: '80.59.197.13',
    port: '61616',
    user: 'jonathan',
    password: 'super3',
    database: 'MyBookList'
});

router.get('/getLibrosFromTitulo', (req, res) => {
  const titulo = "%" + req.query.titulo.toUpperCase() + "%";
  conn.query(`SELECT l.id, l.titulo_ingles, l.titulo_original, l.isbn, l.portada, l.fecha_publicacion, l.sinopsis, l.puntuacion, e.nombre 'editorial', g.nombre 'genero'  FROM Libros l  INNER JOIN Editoriales e ON l.editorial = e.id  INNER JOIN Libros_Generos lg ON l.id = lg.libro  INNER JOIN Generos g ON g.id = lg.genero WHERE UPPER(l.titulo_ingles) LIKE ? OR UPPER(l.titulo_original) LIKE ? GROUP BY l.id ORDER BY l.id ASC;`, [titulo, titulo], (err,rows) => {
    if(err){
      console.log(err);
    } else if (Object.keys(rows).length === 0){
      console.log("0 objetos");
    } else {

      const idLibros = [];
      rows.forEach(row => {
        idLibros.push(row.id);
      });
      getAutoresFromLibros(idLibros, (autores)=>{
        getTotalUsuariosFromLibros(idLibros, (totalUsuarios) => {
          const libros = [];
          for(var i = 0; i< rows.length; i++) {
            var totalUsers = 0;
            for(var k = 0; k < totalUsuarios.length ; k++) {
              if(totalUsuarios[k].id_libro===rows[i].id){
                totalUsers = totalUsuarios[k].totalUsuarios;
              }
            }
            const libro = {
              titulo_ingles: rows[i].titulo_ingles,
              titulo_original: rows[i].titulo_original,
              isbn: rows[i].isbn,
              portada: rows[i].portada,
              fecha_publicacion: rows[i].fecha_publicacion,
              sinopsis: rows[i].sinopsis,
              puntuacion: rows[i].puntuacion,
              editorial: rows[i].editorial,
              genero: rows[i].genero,
              autores: autores[i].autores,
              totalUsuarios: totalUsers
            }
            libros.push(libro);
          }
          res.status(200).json({
            respuesta:libros
          });
        });

      });

    }
  });
});

function getTotalUsuariosFromLibros(idLibros, _callback){
  conn.query("SELECT id_libro, COUNT(*) as totalUsuarios FROM Listas_Libros_Estados WHERE id_libro IN ? GROUP BY id_libro ORDER BY id_libro ASC;", [idLibros], (err, rows) => {
    if(err){
      console.log(err);
    } else if (Object.keys(rows).length === 0) {
      console.log("0 objetos");
    } else {
      const totalUsuarios = [];
      const idsInsertadas = [];
      rows.forEach(row => {
        const libro = {
          id_libro: row.id_libro,
          totalUsuarios: row.totalUsuarios
        }
        idsInsertadas.push(row.id_libro);
        totalUsuarios.push(libro);
      });
      console.log(totalUsuarios);
      _callback(totalUsuarios);
    }
  });
};

function getAutoresFromLibros(idLibros, _callback) {
  conn.query("SELECT libro, nombre, apellidos FROM Personas_Roles_Libros prl INNER JOIN Personas p ON prl.persona=p.id WHERE prl.libro IN ? ORDER BY libro ASC;", [idLibros], (err, rows) => {
    if(err){
      console.log(err);
    } else if (Object.keys(rows).length === 0) {
      console.log("0 objetos");
    } else {
      var AutoresLibro = [];
      rows.forEach(row => {
        const persona = row.nombre + "" + (row.apellidos == null? "" : " "+row.apellidos);
        var autorLibro = new AutorLibro(row.libro);
        var k = 0;
        for(var i = 0; i<AutoresLibro.length; i++) {
          if(AutoresLibro[i].id === row.libro) {
            AutoresLibro[i].addAutor(persona);
          } else {
            k++;
          }
        }
        if(AutoresLibro.length == 0 || k==AutoresLibro.length){
            autorLibro.addAutor(persona);
            AutoresLibro.push(autorLibro);
          }
      });
      _callback(AutoresLibro);
    }
  });
};

class AutorLibro {
  autores = [];
  id;
  constructor(id){
    this.id = id;
  }

  addAutor(autor){
    this.autores.push(autor);
  }
}

router.get('/getLibroFromISBN', (req, res) => {
  conn.query(`SELECT l.id, l.titulo_ingles, l.titulo_original, l.isbn, l.portada, l.fecha_publicacion, l.sinopsis, l.puntuacion, e.nombre 'editorial', g.nombre 'genero'  FROM Libros l  INNER JOIN Editoriales e ON l.editorial = e.id  INNER JOIN Libros_Generos lg ON l.id = lg.libro  INNER JOIN Generos g ON g.id = lg.genero WHERE l.isbn = ?;`, [req.query.isbn], (err,rows) => {
    if(err){
      console.log(err);
    } else if (Object.keys(rows).length === 0){
      console.log("0 objetos");
    } else {
      const idLibro = [rows[0].id];
      getAutoresFromLibros(idLibro, (autores)=>{
        getTotalUsuariosFromLibros(idLibro, (totalUsuarios) => {
          var totalUsers = 0;
            for(var k = 0; k < totalUsuarios.length ; k++) {
              if(totalUsuarios[k].id_libro===rows[0].id){
                totalUsers = totalUsuarios[k].totalUsuarios;
              }
            }
          const libro = {
            titulo_ingles: rows[0].titulo_ingles,
            titulo_original: rows[0].titulo_original,
            isbn: rows[0].isbn,
            portada: rows[0].portada,
            fecha_publicacion: rows[0].fecha_publicacion,
            sinopsis: rows[0].sinopsis,
            puntuacion: rows[0].puntuacion,
            editorial: rows[0].editorial,
            genero: rows[0].genero,
            autores: autores[0].autores,
            totalUsuarios: totalUsers
          };
          res.status(200).json({
            respuesta:libro
          });
        });
      });
    }
  });
});

module.exports = router;
