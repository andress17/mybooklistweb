import { Component, OnInit, Input } from '@angular/core';
import { AnalisisService } from 'src/app/services/analisis.service';
import { Analisis } from 'src/app/models/analisis.model';

@Component({
  selector: 'app-analisis',
  templateUrl: './analisis.component.html',
  styleUrls: ['./analisis.component.css']
})
export class AnalisisComponent implements OnInit {

  listadoAnalisis: Analisis[] = [];
  @Input() idLibro;

  constructor(private analisisService: AnalisisService) { }

  ngOnInit(): void {
    this.analisisService.getAnalisis(this.idLibro).subscribe( resultado => {
      this.listadoAnalisis = resultado.resultado;
    });
    this.analisisService.recargarAnalisis.subscribe( id => {
      if(id) {
        this.analisisService.getAnalisis(id).subscribe( resultado => {
          this.listadoAnalisis = resultado.resultado;
        });
      }
      else {
        this.analisisService.getAnalisis(this.idLibro).subscribe( resultado => {
          this.listadoAnalisis = resultado.resultado;
        });
      }
    });
  }

  //Metodo para convertir el objeto Date en un string
  public parsearFecha(fecha: string): string {
    const fechaParseada: Date = new Date(fecha);

    const fechaFinal: string = fechaParseada.getDay() + '/' + fechaParseada.getUTCMonth() + '/' + fechaParseada.getFullYear();
    return fechaFinal;
  }
}
