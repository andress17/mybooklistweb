import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InicioComponent } from './inicio/inicio.component';
import { FichaLibroComponent } from './ficha-libro/ficha-libro.component';
import { RegistroComponent } from './auth/registro/registro.component';
import { ListaComponent } from './lista/lista.component';
import { PerfilComponent } from './perfil/perfil.component';
import { EditarPerfilComponent } from './perfil/editar-perfil/editar-perfil.component';

const routes: Routes = [
    { path: '', component: InicioComponent },
    { path: 'libro/:isbn', component: FichaLibroComponent },
    { path: 'registro', component: RegistroComponent },
    { path: 'usuario/:usuario/lista', component: ListaComponent },
    { path: 'usuario/:usuario', component: PerfilComponent },
    { path: 'usuario/:usuario/editar', component: EditarPerfilComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
