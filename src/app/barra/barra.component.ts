import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { Usuario } from '../models/usuario.model';

@Component({
  selector: 'app-barra',
  templateUrl: './barra.component.html',
  styleUrls: ['./barra.component.css']
})
export class BarraComponent implements OnInit {

  isLogin: boolean = false;
  usuario: Usuario;

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    //Listener para saber si el usuario esta logueado
    this.usuarioService.usuarioLogin.subscribe((login) => {
      this.isLogin = login;
      this.usuario = this.usuarioService.usuario;
    })
  }

}
