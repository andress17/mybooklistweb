import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.component.html',
  styleUrls: ['./editar-perfil.component.css']
})
export class EditarPerfilComponent implements OnInit {

  form: FormGroup;
  isLogin: boolean;
  biografia: string;

  constructor(private router: Router, private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.usuarioService.
    usuarioLogin.subscribe(login => {
      this.isLogin = login
      this.form = new FormGroup({
        biografia: new FormControl(this.usuarioService.usuario.biografia),
        imagen: new FormControl(this.usuarioService.usuario.imagen)
      });
      this.usuarioService.getUsuario(this.usuarioService.usuario.usuario).subscribe(usuario => {
        this.biografia = usuario.biografia;
        this.form.patchValue({
          biografia: this.biografia,
          imagen: this.usuarioService.usuario.imagen
        })
      });
    });
    this.isLogin = this.usuarioService.usuario? true: false;
    if(this.isLogin) {
      this.form = new FormGroup({
        biografia: new FormControl(this.usuarioService.usuario.biografia),
        imagen: new FormControl(this.usuarioService.usuario.imagen)
      });
      this.usuarioService.getUsuario(this.usuarioService.usuario.usuario).subscribe(usuario => {
        this.biografia = usuario.biografia;
        this.form.patchValue({
          biografia: this.biografia,
          imagen: this.usuarioService.usuario.imagen
        })
      });
    }
  }

  onSubmit() {
    this.usuarioService.modificarPerfil(this.form.get('biografia').value, this.form.get('imagen').value).subscribe(respuesta => {
      this.router.navigate(['/usuario', this.usuarioService.usuario.usuario]);
    });
  }
}
