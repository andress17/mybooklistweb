import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class AuthService {

  constructor(private http: HttpClient) {}


  registro(nombre: string, apellidos: string, nombre_usuario: string, contrasenya: string, email:string, fechaNacimiento: string, imagen: string, biografia: string) {
    let usuario;
    if (!imagen || imagen.trim() === '') {
      usuario = {nombre: nombre, apellidos: apellidos, usuario: nombre_usuario, contrasenya: contrasenya, email:email, fecha_nacimiento: fechaNacimiento, biografia: biografia}
    }
    else {
      usuario = {nombre: nombre, apellidos: apellidos, usuario: nombre_usuario, contrasenya: contrasenya, email:email, imagen: imagen, fecha_nacimiento: fechaNacimiento, biografia: biografia}
    }
    return this.http.post<{ resultado: any }>('http://localhost:3000/user/registro', usuario);
  }

  autenticar(nombre_usuario: string, contrasenya: string) {
    const usuario = {usuario: nombre_usuario, contrasenya: contrasenya};
    return this.http.post<{ mensaje: string, token: string, expiracion: string, id: number, nombre: string, imagen: string }>('http://localhost:3000/user/autenticar', usuario);
  }

  cerrarSesion(id: number, token: string) {
    const usuario = {
      usuario: id,
      token: token
    }
    return this.http.post<{ mensaje: string }>('http://localhost:3000/user/cerrarSesion', usuario);
  }
}
