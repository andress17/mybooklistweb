import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario.model';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class UsuarioService {

  usuario: Usuario;
  usuarioLogin = new Subject<boolean>();

  constructor(private http: HttpClient) {}

  getUsuario(usuario: string): any {
      return this.http.get<{ resultado: Usuario }>('http://localhost:3000/user/perfil/' + usuario);
  }

  getSeguidor(id: number) {
    const datos = { seguido: id };
    return this.http.post<{siguiendo:boolean}>('http://localhost:3000/user/seguidor/', datos);
  }

  seguir(id: number) {
    const datos = { seguido: id };
    return this.http.post<{ mensaje: string }>('http://localhost:3000/user/seguir/', datos);
  }

  dejarSeguir(id: number) {
    const datos = { seguido: id };
    return this.http.post<{ mensaje: string }>('http://localhost:3000/user/dejarseguir/', datos);
  }

  modificarPerfil(biografia: string, imagen: string) {
    const datos = { biografia: biografia, imagen: imagen };
    return this.http.post<{ mensaje: string }>('http://localhost:3000/user/actualizarPerfil/', datos);
  }
}
