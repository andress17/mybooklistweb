import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LibroSeguidores } from 'src/app/models/libroseguidores.model';
import { Usuario } from '../models/usuario.model';

@Injectable({providedIn: 'root'})
export class LibroSeguidoresService {

  constructor(private http: HttpClient) {}

  getLibrosSeguidores(isbn: string){
    return this.http.post<LibroSeguidores>('http://localhost:3000/user/getSeguidoresFromLibro', {isbn:isbn});
  }
}
