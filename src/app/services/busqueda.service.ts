import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Busqueda } from '../models/busqueda.model';

@Injectable({providedIn: 'root'})
export class BusquedaService {

  constructor(private http: HttpClient) {}

  getBusqueda(busqueda: string) {
    const datos = {busqueda: busqueda};
    return this.http.post<{ resultado: Array<Busqueda> }>('http://localhost:3000/busqueda', datos);
  }
}
