export interface Actividad {
  id: number;
  usuario: string;
  actividad_id: number;
  actividad: string;
  titulo: string;
  puntuacion: number;
  isbn: string;
}
