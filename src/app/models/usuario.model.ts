export interface Usuario {
  id: number;
  usuario: string;
  imagen: string;
  biografia?: string;
  fecha_nacimiento?: string;
}
