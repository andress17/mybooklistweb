export interface Analisis {
  id_usuario: number;
  usuario: string;
  imagen: string;
  fecha_creacion: string;
  fecha_ultima_modificacion?: string;
  contenido: string;
}
