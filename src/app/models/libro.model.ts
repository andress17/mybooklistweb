export interface Libro {
   id: number;
   titulo_ingles: string;
   titulo_original: string;
   portada: string;
   isbn: string;
   fecha_publicacion: string;
   sinopsis: string;
   puntuacion: number;
   editorial: string;
   autor?: string;
   ilustrador?: string;
   total: number;
}
