export interface Busqueda {
  nombre: string;
  imagen: string;
  isbn?: string;
  tipo: number;
}
