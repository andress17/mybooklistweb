import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import  * as moment from 'moment';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  form: FormGroup;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      nombre: new FormControl(null , { validators: [Validators.required, Validators.minLength(3), Validators.maxLength(255)] }),
      apellidos: new FormControl(null , { validators: [Validators.required, Validators.minLength(3), Validators.maxLength(255)] }),
      usuario: new FormControl(null , { validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)] }),
      contrasenya: new FormControl(null , { validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)] }),
      email: new FormControl(null , { validators: [Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.email] }),
      fecha_nacimiento: new FormControl(null , { validators: [Validators.required, this.fechaInvalida] }),
      biografia: new FormControl(null),
      imagen: new FormControl(null)
    });
  }

  // Validador personalizado para comprobar si la fecha es invalida.
  fechaInvalida(control: FormControl): {[key: string]: any} | null {
    if(control.value && control.value != '') {
      const fecha: Date = moment(control.value, 'YYYY/MM/DD').toDate();
      if(fecha.getFullYear() > (new Date().getFullYear() - 130)) {
        if (fecha.getTime() < Date.now()) {
          return null;
        }
        else {
          return {mensaje: 'Fecha invalida'};
        }
      }
      else {
        return {mensaje: 'Fecha invalida'};
      }
    }
    else {
      return {mensaje: 'Fecha invalida'};
    }
  }

  onSubmit() {
    this.form.markAsTouched({ onlySelf: true });
    this.authService.registro(
      this.form.get('nombre').value,
      this.form.get('apellidos').value,
      this.form.get('usuario').value,
      this.form.get('contrasenya').value,
      this.form.get('email').value,
      moment(this.form.get('fecha_nacimiento').value, 'YYYY/MM/DD').toDate().toString(),
      this.form.get('imagen').value,
      this.form.get('biografia').value
    ).subscribe(respuesta => {
      this.router.navigate(['/']);
    });
  }
}
