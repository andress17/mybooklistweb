import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    if(localStorage.getItem('token')) {
      const modifiedReq = req.clone({
        headers: new HttpHeaders().set('Authentication', localStorage.getItem('token'))
      });
      return next.handle(modifiedReq);
    }
    else {
      return next.handle(req);
    }
  }
}
