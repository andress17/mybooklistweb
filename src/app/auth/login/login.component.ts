import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hideLogin = true;
  form: FormGroup;
  isLogin: boolean = false;
  imagenUsuario: string;
  constructor(private router: Router, private authService: AuthService, private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      usuario: new FormControl(null , { validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)] }),
      contrasenya: new FormControl(null , { validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)] })
    });
    this.usuarioService.usuarioLogin.subscribe(login => {
      this.isLogin = login;
    });
    if(localStorage.getItem('token')) { // Comrpobacion de si existe un token guardado en almacenamiento local del navegador.
      this.autoLogin();
    }
  }

  onIconClick() {
    this.hideLogin = !this.hideLogin; // Mostrar o ocultar el login si el usuario ha hecho click
  }

  onRegistro() {
    this.router.navigate(["/registro"]);
  }

  onSubmit() {
    this.form.markAsTouched({ onlySelf: true }); // Marcar el formulario como modificado cuando se envie para que marque los errores.
    this.authService.autenticar(this.form.get('usuario').value, this.form.get('contrasenya').value).subscribe(respuesta => {
      if(respuesta.hasOwnProperty('token')) {
        this.usuarioService.usuario = { id: respuesta.id, usuario: this.form.get('usuario').value, imagen: respuesta.imagen };
        this.imagenUsuario = respuesta.imagen;
        localStorage.setItem('token', respuesta.token); // recoger el token que llega de la peticion y guardarlo en la memoria local del navegador.
        localStorage.setItem('expiracion', respuesta.expiracion);
        this.usuarioService.usuarioLogin.next(true);
        this.form.reset({});
      }
    });
  }

  onCerrarSesion() {
    this.authService.cerrarSesion(this.usuarioService.usuario.id, localStorage.getItem('token')).subscribe( respuesta => {
      localStorage.clear(); // Eliminar todas las variables de la memoria local del navegador.
      this.usuarioService.usuarioLogin.next(false);
      this.router.navigate(["/"]);
    });
  }

  // Metodo para iniciar sesion automaticamente si el usuario tiene u ntoken valido guardado en el alacenamiento local
  autoLogin() {
    this.authService.autenticar(null, null).subscribe(respuesta => {
      if(respuesta.token) {
        this.usuarioService.usuario = { id: respuesta.id, usuario: respuesta.nombre, imagen: respuesta.imagen };
        this.imagenUsuario = respuesta.imagen;
        localStorage.setItem('token', respuesta.token);
        localStorage.setItem('expiracion', respuesta.expiracion);
        this.usuarioService.usuarioLogin.next(true);
      }
      else {
        localStorage.clear();
        this.usuarioService.usuarioLogin.next(false);
      }
    });
  }
}
